#include "NameList.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdio>


using std::ifstream;
using std::stringstream;
using std::istringstream;

using std::ifstream;
using std::cerr;

NameList::NameList()
{
    //the random device that will seed the generator
    std::random_device seeder;
    //then make a mersenne twister engine
    rng.seed(seeder());
}

NameList::~NameList()
{
    //Do nooothing ;-D
}

void NameList::addName(string name)
{
    this->nameList.push_back(name);
}

int NameList::getSize()
{
    return this->nameList.size();
}

void NameList::loadFile(string fileName)
{
    //Open CSV file to buffer
    ifstream infile;

    infile.open(fileName);

    while (infile)
    {
        string s;
        if (!getline( infile, s )) break;

        istringstream ss( s );

        while (ss)
        {
            string s;
            if (!getline( ss, s, ',' )) break;
            this->nameList.push_back( s );
        }
     }

     if (!infile.eof())
     {
       cerr << "Something wrong - oh dear!\n";
     }

    //Close File
     infile.close();

}

/* Note, this method provides the name to be returned, the value return is
 * a valid iterator.  Any previous iterator will be invalidated by the
 * erase.
 */
NameList::iterator NameList::pickName(string& name)
{
    int rangeMax = this->nameList.size()-1;
    std::uniform_int_distribution<int> gen(0, rangeMax); // uniform, unbiased

    int randomIndex = gen(rng);

    name = *(nameList.begin()+randomIndex);
    return this->nameList.erase(nameList.begin()+randomIndex);
}
